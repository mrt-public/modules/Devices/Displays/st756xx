ST756xx
=======


This module provides a driver for the ST756xx series of displays. The driver is written in C


Example
-------

.. code:: C

    #include "st756xx.h"
    #include "Utilites/GFX/gfx/gfx.h"

    st756xx_t display;

    void main() {

        //Initialize HW interface
        st756xx_init_hw(&display, &spi, &gpio_cs, &gpio_cmd);

        //Initialize display canvas
        st756xx_init_gfx(&display, 128, 64);


        //Set pen with stroke of 1 pixel and color black
        gfx_set_pen(&display.mCanvas, 1, GFX_COLOR_BLACK);

        //draw a 123x59 rectangle at x,y = 5,5 and fill it in
        gfx_draw_rect(&display.mCanvas, 5, 5, 123, 59, GFX_OPT_FILL);

        //Refresh the display with the canvas buffer
        st756xx_refresh(&display);
    }