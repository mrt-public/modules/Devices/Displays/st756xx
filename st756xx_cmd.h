
/**
 * @file st756xx_cmd.h
 * @brief Command definitions for ST756xx display driver
 * @author Jason Berger
 * @date 07/2/2024
 
 */

/*******************************************************************************
  Commands                                                                              
*******************************************************************************/

#define ST756XX_CMD_DISP_ON                                       0xAF /* Display On */
#define ST756XX_CMD_DISP_OFF                                      0xAE /* Display Off */

#define ST756XX_CMD_START_LINE(x)                                 (0x40 | (x & 0x3F)) /* Display Start Line */
#define ST756XX_CMD_PAGE_ADDR(x)                                  (0xB0 | (x & 0x0F)) /* Page Address */
#define ST756XX_CMD_COL_ADDR_MSB(x)                               (0x10 | (x & 0x0F)) /* Column Address High */
#define ST756XX_CMD_COL_ADDR_LSB(x)                               (0x00 | (x & 0x0F)) /* Column Address Low */
#define ST756XX_CMD_SEG_DIR(x)                                    (0xA0 | (x & 0x01)) /* Segment Direction */
#define ST756XX_CMD_RESET                                         0xE2 /* Reset */




